package clases;

public class Administracion extends Puestos {

	String estudios;
	int salarios, antiguead;
	
	public Administracion(String dni, String nombre, String apellido,int salario, String estudios, int antiguead) {
		super(dni, nombre, apellido, salario);
		this.estudios = estudios;
		this.salario = salario;
		this.antiguead = antiguead;
		this.tipo_puesto = 1;
	}
	
	public Administracion() {}
	

	@Override
	public String toString() {
		return "Administracion [dni=" + super.dni + ", nombre=" + super.nombre + ", apellido=" + super.apellido + 
				", estudios=" + estudios + ", salarios=" + super.salario + ", antiguead=" + antiguead + "]";
		
	}
}