package DAM;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import java.awt.GridLayout;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;
import javax.swing.JSlider;

public class Prueba1 {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	
	public static void main(String[] args) {		
		EventQueue.invokeLater(	//Cola de eventos, si no lo ponemos se abrira y cerrara en un instante la interfaz y no se mantendra
				new Runnable() { /* Necesitamos implementar run dentro de Runnable que es una interfaz abstracta, para ahorrarnos herencias
					haremos una instancia sin guardarla ya que es abstracta y dentro definimos el metodo run */
					public void run() {
						try {
							Prueba1 window = new Prueba1();
							window.frame.setVisible(true);	//	Mantener el foco en la interfaz para que sea visible
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
	}

	/**
	 * Create the application.
	 */
	public Prueba1() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(0, 0, 450, 300);	// Indicamos la posicion de la interfaz y su tamaño
		frame.setLocationRelativeTo(null);	//	Para centrar la interfaz en el centro de la pantalla
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel textoPrueba = new JLabel("HOLA MUNDO!!");
		textoPrueba.setBounds(100,100,100, 100);
		frame.getContentPane().add(textoPrueba);
	}
}
