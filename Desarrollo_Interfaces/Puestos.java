package clases;

public abstract class Puestos {
	
	String dni, nombre, apellido;
	int salario, tipo_puesto;
	
	public Puestos(String dni, String nombre, String apellido, int salario) {
		this.dni = dni;
		this.nombre = nombre;
		this.apellido = apellido;
		this.salario = salario;
	}
	
	public Puestos() {}
	
}