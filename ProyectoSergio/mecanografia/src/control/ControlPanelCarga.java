package control;

import java.awt.event.*;

import javax.swing.JProgressBar;
import javax.swing.Timer;
import view.*;

public class ControlPanelCarga{
	public static int i=1;
	private Timer tiempoBarraCarga;
	
	public ControlPanelCarga(JProgressBar barra) {			
		controlBarra(barra);
		tiempoBarraCarga.start();
	}
	
	
	public void controlBarra(JProgressBar barraCarga) {
		tiempoBarraCarga = new Timer(1000, new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(i==6) {
					tiempoBarraCarga.stop();
				}
				barraCarga.setValue(i);
				i++;
			}
		});
	}
	
}
