package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;

public class MarcoLogin extends JFrame {
	private JTextField textField;
	private JPasswordField passwordField;
	
	/**
	 * Metodo constructor para aplicar las caracteristicas del frame del login
	 * **/
	public MarcoLogin() {
		
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(400, 600);
		setLocationRelativeTo(null);
		
		PanelLogin panel2 = new PanelLogin();
		getContentPane().add(panel2);
		setVisible(true);
	}
}
