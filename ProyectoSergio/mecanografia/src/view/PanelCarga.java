package view;

import javax.swing.*;
import control.*;

public class PanelCarga extends JPanel {

	public JProgressBar barraCarga;
	/**
	 * Create the panel.
	 */
	public PanelCarga() {
		this.setSize(600,600);
		barraCarga = new JProgressBar(0, 6);
		barraCarga.setBounds(100, 512, 400, 14);
		barraCarga.setVisible(true);
		setLocation(400, 200);
		setLayout(null);
		add(barraCarga);
		ControlPanelCarga valorBarra = new ControlPanelCarga(barraCarga);
		
		JLabel lblNewLabel = new JLabel("Cargando...");
		lblNewLabel.setBounds(266, 537, 72, 14);
		add(lblNewLabel);
	}
}
