package view;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import control.*;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;
import main.*;

public class MarcoCarga extends JFrame {

	private JFrame ptr = this;
	private static Timer time;
	private int i=0;
	public MarcoCarga(JFrame marco2) {
		
		setResizable(false);
		setUndecorated(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(600, 600);
		setLocationRelativeTo(null);
		
		
		PanelCarga panel1 = new PanelCarga();
		add(panel1);
		setVisible(true);
		
		time = new Timer(1000, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {				
				if(i == 6) 
					iniciarDestructor(marco2);
				i++;
			}
		});
		time.start();
		
	}
	
	
	public void iniciarDestructor(JFrame marco2) {
		marco2 = new MarcoLogin();
		dispose();
	}
}
