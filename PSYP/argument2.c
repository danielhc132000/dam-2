#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 20


int comprobar_argumentos(int *argc){

    if(*argc <= 1){
        fprintf(stderr, "Error, agregar minimo un argumento.\n");
        return EXIT_FAILURE;
    }
}


char *palabra(int *argc, char *argv[]){

    static char word[MAX] = " ";

    for(int i=1; i<*argc; i++){
      if(strlen(word) < strlen(argv[i]))
        strcpy(word, argv[i]);
    }

    return word;
}


int main(int argc, char *argv[]){

    comprobar_argumentos(&argc);

    printf("%s\n", palabra(&argc, argv));

    return EXIT_SUCCESS;
}
