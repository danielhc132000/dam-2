#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <stdio.h>
#include <stdlib.h>

#define NUM_CHILD 5


int doSomething(void){
    int ret;

    srand(getpid());
    ret = (rand() % 256);
    printf("HIJO: PID = %d, valor aleatorio calculado %d \n", getpid(), ret);
    
    return ret;
}


int main(void){

    pid_t pidI;
    int status;

    for(int i=0; i< NUM_CHILD; i++){
        pidI = fork();
        if(pidI > 0){
            continue;
        }else if(pidI == 0){
            exit(doSomething());    
        }else{/*Error*/}
    }

    for(int i=0; i<NUM_CHILD; i++){
        pidI = wait(&status);
        printf("PADRE de PID = %d, hijo de PID %d terminado, status = %d", getpid(), getppid(), WEXITSTATUS(status));
    }

    while(1){
        sleep(2);
    } 


    return EXIT_SUCCESS;
}
