#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>

int var = 22;

int main(void){
    
    pid_t pid;    

    printf("** proc. PID = %d comienza **\n", getpid());
    pid = fork();
 
    printf("proc. PID = %d, pid = %d ejecutandose \n", getpid(), pid);

    if(pid>0){
        var = 44;
    }else if(pid == 0){
        var = 33;
    }else{  // Error
    }

    
    while(1){
        sleep(2);
        printf("proc. PID = %d, var = %d ejecutandose \n", getpid(), var);
    }


    return 0;
}
