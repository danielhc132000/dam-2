#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 20


int main(int argc, char *argv[]){

    if(argc > 5 || argc < 2){
        fprintf(stderr, "Ingresar entre 1 y 5 argumentos\n");
        return EXIT_FAILURE;
    }

    char *word = *argv;

    for(int i=1; i < argc; i++){
        if(strlen(word) < strlen(argv[i]))
            word = argv[i];
    }

    printf("%s\n", word);
    free(word);

    return EXIT_SUCCESS;
}