#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 20

int main(int argc, char *argv[]){

    if(argc <= 1){
        fprintf(stderr, "Error, agregar minimo un argumento.\n");
        return EXIT_FAILURE;
    }

    char word[MAX] = " ";

    for(int i=1; i<argc; i++){
      if(strlen(word) < strlen(argv[i]))
        strcpy(word, argv[i]);
    }


    printf("%s\n", word);

    return EXIT_SUCCESS;
}
