#include <stdio.h>
#include <stdlib.h>

void
par_impar(int num){
    system("clear");

    if(num%2==0)
        printf("%i es par\n\n", num);
    else
        printf("%i es impar\n\n", num);
}


int main(){

    short stop;
    int num;

    do{
        printf("Escribe \"fin\" para finalizar el programa\n");
        printf("Numero: ");
        stop = scanf("%i", &num);
        par_impar(num);
    }while(stop != 0);
    
    system("clear");
    return EXIT_SUCCESS;
}