#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 5

int main(int argc, char *argv[]){

  //  Variables

  const char vocales[5] = {'a', 'e', 'i', 'o', 'u'};
  char *word = argv[1];
  char *pointer;
  int num, num_prev=0;

  //Algoritmia

  for(int i=1; i<argc; i++){
    num = 0;
    pointer = argv[i];
    for(int l=0; l<strlen(argv[i]); l++){
      for(int a=0; a<MAX; a++){
        if(pointer[l]==vocales[a]){
          num++;
        }
      }
    }
    
    //  Apunte de palabra

    if(num>num_prev){
        word = argv[i];
        num_prev=num;
    }
    if(num == num_prev){
      printf("Dos palabras tienen el mismo numero de vocales\n");
      return EXIT_SUCCESS;
    }
  }

  //  Output de valores

  printf("%s\n", word);

  
  return EXIT_SUCCESS;
}
