#include <stdio.h>
#include <stdlib.h>

void
suma(double operador1, double operador2){
    double resultado = operador1 + operador2;
    printf("%.2lf + %.2lf = %.2lf", operador1, operador2, resultado);
}

void
resta(double operador1, double operador2){
    double resultado = operador1 - operador2;
    printf("%lf - %lf = %lf", operador1, operador2, resultado);
}

void
multi(double operador1, double operador2){
    double resultado = operador1 * operador2;
    printf("%lf * %lf = %lf", operador1, operador2, resultado);
}

void
divi(double operador1, double operador2){
    double resultado = operador1 / operador2;
    printf("%lf / %lf = %lf", operador1, operador2, resultado);
}

int main(int argc, char *argv[]){

    char operador;
    double operador1, operador2;

    printf("Operacion a realizar: ");
    scanf("%c", &operador);

    printf("Numeros a operar: ");
    scanf("%lf %lf", &operador1, &operador2);

    switch(operador){
        case '+': suma(operador1, operador2);
            break;
        case '-': resta(operador1, operador2);
            break;
        case '*': multi(operador1, operador2);
            break;
        case '/': divi(operador1, operador2);
    };

    return EXIT_SUCCESS;
}