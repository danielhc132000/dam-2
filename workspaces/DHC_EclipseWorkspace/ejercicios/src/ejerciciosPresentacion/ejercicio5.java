package ejerciciosPresentacion;

import java.util.Scanner;

public class ejercicio5 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		System.out.println("Numero a calcular el factorial: ");
		int num = sc.nextInt();
				
		System.out.println("El factorial de " + num + " es: " + factorial(num));
		sc.close();
	}
	
	protected static int factorial(int num) {
		if(num > 1) {	//	Final Case or Base Case
			return num * factorial(num-1);
		}
		return num;
	}
	
}
