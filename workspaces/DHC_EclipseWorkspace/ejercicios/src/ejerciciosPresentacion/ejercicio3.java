package ejerciciosPresentacion;

public class ejercicio3 {

	public static void main(String[] args) {
		
		int[] array = {1,2,3,4,5,6,7,8,9,10};
		int cantidad = 0;
		
		for(int numero : array) {
			if(numero%2==0)
				cantidad++;
		}
		
		System.out.println("El array tiene " + cantidad + " numeros pares");
		
	}

}
