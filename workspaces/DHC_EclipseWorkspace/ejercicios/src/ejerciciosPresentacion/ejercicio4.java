package ejerciciosPresentacion;

import java.util.Scanner;

public class ejercicio4 {

	public static void main(String[] args) {

		Scanner readKeyboard = new Scanner(System.in);
		int total = 0;
		
		int tamaño=0;
		try {
			System.out.println("Tamaño de la matriz: ");
			tamaño = readKeyboard.nextInt();
		}catch (Exception e) {
			System.err.print("Solo se aceptan numero dentro del rango de un int");
			System.exit(1);
		}
		
		int numeros[][] = new int [tamaño][tamaño];
		
		for (int f=0; f<numeros.length; f++) {
			for(int c=0; c<numeros.length; c++) {
			  numeros[f][c] = (int) (Math.random()*100);
			  System.out.print(numeros[f][c] + " ");
			  total += numeros[f][c];
			}
			System.out.println("");
		}
		
		
		System.out.println("\nEl resultado de la suma es: " + total);
		readKeyboard.close();
	}
}