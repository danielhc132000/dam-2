package clases;

public class Persona{
	
	String dni, nombre, apellido;
	int tipo_puesto;
	
	public Persona(String dni, String nombre, String apellido) {
		this.dni = dni;
		this.nombre = nombre;
		this.apellido = apellido;
	}
	
	public Persona() {}
}