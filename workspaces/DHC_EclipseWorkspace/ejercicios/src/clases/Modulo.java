package clases;

public class Modulo {
	String nombre;
	Profesor profesor;
	int numero_horas;
	boolean es_convalidable;
	
	Modulo(String nombre, Profesor profesor, int numero_horas, boolean es_convalidable){
		this.nombre = nombre;
		this.profesor = profesor;
		this.numero_horas = numero_horas;
		this.es_convalidable = es_convalidable;
	}
}