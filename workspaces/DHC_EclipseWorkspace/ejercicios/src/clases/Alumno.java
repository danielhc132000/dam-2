package clases;

import java.util.ArrayList;

public class Alumno extends Persona {
	String fecha_nacimiento, sexo;
	boolean es_repetidor;
	ArrayList<Modulo> modulos; 
	
	public Alumno(String dni, String nombre, String apellido, String fecha_nacimiento, String sexo, boolean es_repetidor, ArrayList<Modulo> modulos) {
		super(dni, nombre, apellido);
		this.fecha_nacimiento = fecha_nacimiento;
		this.sexo = sexo;
		this.es_repetidor = es_repetidor;
		this.modulos = modulos;
	}
}