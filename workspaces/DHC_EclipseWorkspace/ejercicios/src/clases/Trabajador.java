package clases;

public class Trabajador extends Persona {

	int salario, id_profesor;
	static int num_profesor=+1;
	
	public Trabajador(String dni, String nombre, String apellido, int salario) {
		super(dni, nombre, apellido);
		this.salario = salario;
		this.id_profesor = Trabajador.num_profesor;
	}
	
	public Trabajador() {}
	
}