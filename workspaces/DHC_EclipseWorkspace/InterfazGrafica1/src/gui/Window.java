package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JProgressBar;
import java.awt.Toolkit;
import javax.swing.JSeparator;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class Window extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;


	public Window() {
		
		setTitle("Area del Triangulo\r\n");
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\cfgs.LAB35-PC04.000\\Documents\\DHC_EclipseWorkspace\\InterfazGrafica1\\thropy_cup_winner_medal_sport_icon_228609.ico"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 500, 500);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 128, 64));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblBase = new JLabel("Base");
		lblBase.setFont(new Font("Calibri", Font.BOLD, 20));
		lblBase.setBounds(69, 178, 73, 20);
		contentPane.add(lblBase);
		
		JLabel lblTitle = new JLabel("Area del triangulo");
		lblTitle.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 30));
		lblTitle.setBounds(131, 53, 241, 51);
		contentPane.add(lblTitle);
		
		JLabel lblAltura = new JLabel("Altura");
		lblAltura.setFont(new Font("Calibri", Font.BOLD, 20));
		lblAltura.setBounds(69, 237, 73, 20);
		contentPane.add(lblAltura);
		
		textField = new JTextField();
		textField.setBounds(205, 178, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(205, 237, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double base = Double.parseDouble(textField.getText());
				double altura = Double.parseDouble(textField_1.getText());
				double area = (base * altura) / 2;
			}
		});
		
		btnCalcular.setBounds(202, 327, 89, 23);
		contentPane.add(btnCalcular);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(20, 22, 63, 22);
		contentPane.add(menuBar);
		
		JMenu mnMenu = new JMenu("Opciones");
		mnMenu.setBounds(10, 11, 63, 20);
		contentPane.add(mnMenu);
		
		JMenuItem mntmExitItem = new JMenuItem("Salir");
		mntmExitItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(ABORT);
			}
		});
		
		mnMenu.add(mntmExitItem);		
	}
}