package insert;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class main {

	public static void main(String[] args) {
		
		String jdbc = "jdbc:mysql://";
		String host = "localhost";
		String port = "3306";
		String baseDatos = "conexion_jdbc";
		String urlConection = jdbc + host+ ":" + port + "/" + baseDatos;
		String usr = "root";
		String pwd = "";
		
		Connection miConexion = null;
		
		PreparedStatement sentencia = null;
		
		try {
			// Establecer Conexion - DriverManager
			
			miConexion = DriverManager.getConnection(urlConection, usr, pwd);
			System.out.println("\n¡Conexion Abierta!\n");
			
			// Ejecutar Sencencia
			
			String sentenciaSQL = 
				("INSERT INTO ALUMNO (ID_ALUMNO, NOMBRE_ALUMNO, APELLIDO1_ALUMNO, APELLIDO2_ALUMNO, DNI_ALUMNO, EMAIL_ALUMNO, TELEFONO_ALUMNO)"
						+ "VALUES(?,?,?,?,?,?,?)");
			
			sentencia = miConexion.prepareStatement(sentenciaSQL);
			
			sentencia.setInt(1, 1);
			
		}catch(SQLException e) {
			System.err.println("Error en la base de datos");
		}finally {
			try {
				miConexion.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
