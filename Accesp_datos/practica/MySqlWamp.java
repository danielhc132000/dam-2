package practica;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MySqlWamp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String jdbc = "jdbc:mysql://";
		String host = "localhost";
		String port = "3306";
		String baseDatos = "conexion_jdbc";
		String urlConection = jdbc + host+ ":" + port + "/" + baseDatos;
		String usr = "root";
		String pwd = "";
		
		Connection miConexion = null;
		
		try {
			// Establecer Conexion - DriverManager
			
				miConexion = DriverManager.getConnection(urlConection, usr, pwd);
				System.out.println("\n¡Conexion Abierta!\n");
			
			// Crear Objeto Sentencia - Statement
				
				Statement miStatment = miConexion.createStatement();
			
			// Ejecutar Sentencias - ExecuteQuery - ResultSet
				
				String sentencia = ("SELECT * FROM Alumno");
				ResultSet miResultado = miStatment.executeQuery(sentencia);	
			
			// Leer Resultado Consulta
				
				while(miResultado.next()) {
					System.out.println(miResultado.getString("id_alumno")
							+ " |" + 
							miResultado.getString("apellido1_alumno")
							+ " |" +
							miResultado.getString("apellido2_alumno")
							+ " |" +
							miResultado.getString("nombre_alumno")
							+ " |" +
							miResultado.getString("dni_alumno")
							+ " |" + 
							miResultado.getString("email_alumno")
							+ " |" +
							miResultado.getString("telefono_alumno")
							+ " |");
				}
				
				
		}catch(SQLException e){
			System.out.println("Error en la base de datos");
		}catch(Exception e){
			System.out.println("Error desconocido");
		}finally {
				try {
					miConexion.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				System.out.println("\n¡Conexion Cerrada!\n");
			}
		}
		

}